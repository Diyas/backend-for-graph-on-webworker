const express = require("express");
const app = express();
const temperature = require("./temperature.json")
const precipitation = require("./precipitation.json")

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'DELETE, POST, GET, PUT, OPTIONS');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        return res.status(200).end();
    }
    next();
});

app.get('/api/temperature', function (req, res) {
    res.json(temperature)
});

app.get('/api/precipitation', function (req, res) {
    res.json(precipitation)
});

app.listen(3000, function () {
    console.log('port 3000!');
});